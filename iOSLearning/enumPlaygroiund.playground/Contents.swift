import UIKit
import CoreGraphics
// sharingPlatform is the name of the enum and String is the datatype of the raw value
enum sharingPlatform: String, CaseIterable {
    case facebook  = "Sharing via facebook"
    case twitter   = "Sharing via Twitter"
    case instagram = "Sharing via instagram"
    case whatsapp  = "sharing via Whatsapp"
}
func shareMyFile (on platform: sharingPlatform){
    switch platform{
    case .facebook:
        print(platform.rawValue)
    case .twitter:
        print(platform.rawValue)
    case .instagram:
        print(platform.rawValue)
    case .whatsapp:
        print(platform.rawValue)
    }
}
shareMyFile(on: .facebook)
print(sharingPlatform.allCases.count)
for cases in sharingPlatform.allCases{
    print(cases,"\t",separator: "...",terminator: " ")
    print(cases.rawValue)
}

