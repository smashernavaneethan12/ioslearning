//
//  MyLearningApp.swift
//  MyLearning
//
//  Created by navaneeth-pt4855 on 08/01/22.
//

import SwiftUI

@main
struct MyLearningApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
