//
//  ContentView.swift
//  MyLearning
//
//  Created by navaneeth-pt4855 on 08/01/22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hi!!!").padding().background(Color.black).foregroundColor(Color.white)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
