////import UIKit
////typealias integer = Int
////// inout parameters
////func swapUsingInOut(a: inout integer ,b: inout integer) {
////    var temp: Int?
////    temp = a
////    a = b
////    b = temp!
////}
////func swapUsingInOut(a:Int,b:Int) -> (a1:Int,b1:Int,a:Int,b:Int){
////    var temp: Int?
////    var a1:Int = a
////    var b1:Int = b
////    temp = a1
////    a1 = b1
////    b1 = temp!
////    return (a1,b1,a,b)
////}
////var a:Int = 10
////var b:Int = 5
////print("Before swap A is \(a) and B is \(b)")
////swapUsingInOut(a: &a, b: &b)
////print("After swap A is \(a) and B is \(b)")
////let res = swapUsingInOut(a: a, b: b)
////// a is changed to 5 and b is changed to 10
////print("A1 is \(res.a1)  b1 is \(res.b1)  A is \(res.a) B is \(res.b)")
//let http404Error = (404,"HTTPError")
//let (statusCode, statusMessage) = http404Error
//print("The status code is \(statusCode)")
//// Prints "The status code is 404"
//print("The status message is \(statusMessage)")
//// Prints "The status message is Not Found”
//let studentDetails = (Name: "Navaneethan",Age: 21,DOB: "12-07-2000")
//print("Name of the student is \(studentDetails.Name)")
//print("Age of the student is \(studentDetails.1)")
enum Rank: Int {
    case ace = 1
    case two, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king

//    func simpleDescription() -> String {
//        switch self {
//        case .ace:
//            return "ace"
//        case .jack:
//            return "jack"
//        case .queen:
//            return "queen"
//        case .king:
//            return "king"
//        default:
//            return 
let ace = Rank.jack
let aceRawValue = ace.rawValue
print(ace)
print(
