import UIKit
import Darwin
import Foundation
import CoreGraphics

// availability check
if #available(iOS 10, macOS 10.12, *) {
    // Use iOS 10 APIs on iOS, and use macOS 10.12 APIs on macOS
} else {
    // Fall back to earlier iOS and macOS APIs
}

//assertion
let a:Int=5
//assert(condition,"Failing message")
assert(a>4,"You must enter a value greater than 4 to continue")
precondition(a<6, "A must be lesser than 6")
print("Hello")

//nil coleasing operator
var b:Int? = nil
let c = 10
let res = b ?? c
print(res)

//range
var range = 5...
print(range.contains(1))

//multiline string
let string = """
    Hello I am currently working on swift programming language.
    It is a powerful tool for developing ios apps!!! \u{1F496}
"""
print(string)
 
//typecasting
let str = ""
var cd = Int(str)
let defaultv:Int = 0
print(cd ?? "error") // nil coleasing operator

var input = "hello"
input.insert(contentsOf: "world ", at: input.startIndex)
print(input)

//array
//var newarray = Array(repeating: 20, count: 5)
var newarray = [1,2,5,3,6,1,7,8,3,4,9]
newarray.append(20)
//newarray = []
//newarray = [Int]()
newarray.sort(by: {(s1:Int,s2:Int)-> Bool in return s1 > s2})
print(newarray)
if newarray.isEmpty {
    print("Array is empty")
}
//set
var myArray = ["hello","my","var","third","a"]
myArray.remove(at: myArray.index(myArray.startIndex, offsetBy: 2))
print(myArray.sorted())
var mySet:Set = ["hello","my","name","is","swift"]
mySet.remove(at: mySet.index(mySet.startIndex, offsetBy: 2))
print(mySet)
var mySet1:Set = ["hello","my","name","is","swift"]
print(mySet.intersection(mySet1))
print(mySet==mySet1) // false because removed one element from set1

//dictionary
var myDic = [String:String]() //empty dic
var myDic2:[Int:String] = [:]  //is also an empty dictionary
print(myDic.count)
myDic = ["b":"apple","a":"ball","c":"cat"] //values in dic are always optional
print(myDic["a"]!) //unwrapping the optional datatype
print(myDic.count)
let someValue = myDic.updateValue("balloon", forKey: "b") /*This updateValue function returns the previous value before updating and if there is no value for that key it returns nil */
print(someValue ?? "It's a nil value") /*removeValue func is also similar to updateValue func but it removes the value for a given key and returns the removed value */
print(myDic)
for (letter,name) in myDic{
    print("Key is \(letter) and value is \(name)")
}
for letters in myDic.keys{
    print(letters)
}
print([String](myDic.keys).sorted())

//scanning a var
if let scan = readLine(){
    print(scan)
}

//for in stride open range and closed range
let endRange = 6
for i in 1...endRange{
    print("\(i)",terminator:" ") // closed range
}
print("\n")
for i in stride(from: 2, to: endRange, by: 2){
    print(i,terminator: " ")  // open range doesn't include endrange
}
print("\n")
for i in stride(from: 2, through: endRange, by: 2){
    print(i,terminator: " ") //closed range includes endrange
}
print("\n")
var sCase = "random"

// Switch case in swift is non-implicit fallthrough
switch sCase{
case "Hello","random":  // two cases having same function (Hello and random case) also known as compound cases
    print(sCase)
    fallthrough   // it's like falling to next case in C language
case "bye":       // we can not leave a case without a statement
    print("bye")
default:
    print("default")
}
// range in switch case
let myInt = 10
switch myInt{
case 1..<10:
    print(myInt)
case 10..<20:
    print("Range from 10 to 19")
default:
    print("default")
}


//tuple
typealias anotherPoint = (String,String)// tuple typealiasing
var myPoint:anotherPoint = ("a","b")
print(myPoint)
var point = (minV : 1,maxV : 2) // tuple initialisation
print(point,point.minV,separator: "..")


let somePoint = (x:1,y:20)
switch somePoint{
case (let x,0):
    print("parallel to y axis by \(x)")
case (0, let y):
    print("parallel to x axis by \(y)")
case (let x,let y):
    print("Is a point at x = \(x) and y = \(y) ")
default:
    print("default")
}


// labelled loops
var i = 10
myWhileLoop : while i>0 {
    for num in 1...5{
        print(num, terminator: " ")
        if num == 4{
            break myWhileLoop
        }
    }
    print("\n")
    i-=1
}
print("\n")

// functions with multiple return
func minMax(array: [Int]) -> (min: Int, max: Int) {
    var currentMin = array[0]
    var currentMax = array[0]
    for value in array {
        if value < currentMin {
            currentMin = value
        } else if value > currentMax {
            currentMax = value
        }
    }
    return (currentMin, currentMax)
}
let bounds = minMax(array: [8, -6, 2, 109, 3, 71])
print("min is \(bounds.min) and max is \(bounds.max)")

func addTwoNum(a:Int,b:Int)->Int {
     a + b     // implicit return
    
}
func addTwoNum(A:Int,B:Int)->Int{
    A+B+2
}
print(addTwoNum(a: 4, b: 2))
print(addTwoNum(A: 2, B: 5))

// nested func

func sayHello(){
    print("Hello")
    
    func sayHi(){
        print("Hai")
    }
   sayHi()  // sayHi can be called within the outer func but can't be called from outside
}
sayHello()  // sayHello can be called from outside

func operate(symbol: String) -> (Int, Int) -> Int {

  // inner function to add two numbers
  func add(num1:Int, num2:Int) -> Int {
    return num1 + num2
  }

  // inner function to subtract two numbers
  func subtract(num1:Int, num2:Int) -> Int {
    return num1 - num2
  }

  let operation = (symbol == "+") ?  add : subtract
  return operation
}

let operation = operate(symbol: "+")
print(operation)
let result = operation(8,3)
print("Result:", result)

var someIbtegers:Int

struct Point {
    var x = 0.0, y = 0.0
}
struct Size {
    var width = 0.0, height = 0.0
}
struct Rect {
    var origin = Point()
    var size = Size()
    var center: Point {
        get {
            let centerX = origin.x + (size.width / 2)
            let centerY = origin.y + (size.height / 2)
            return Point(x: centerX, y: centerY)
        }
        set{// if we write set (newCentre) you must use newCentre to call the parameter, but by default the parameter is newValue.
            origin.x = newValue.x - (size.width / 2)
            origin.y = newValue.y - (size.height / 2)
        }
    }
}
var square = Rect(origin: Point(x: 0.0, y: 0.0),
                  size: Size(width: 10.0, height: 10.0))
let initialSquareCenter = square.center
square.center = Point(x: 15.0, y: 15.0)
print("square.origin is now at (\(square.origin.x), \(square.origin.y))")

// Read only computed property

class MyShape{
    var height:Double
    var width:Double
    init(height:Double, width :Double){
        self.height=height
        self.width=width
    }
    var area: Double{
        return height * width
    }
}

let myRectangle = MyShape(height: 2, width: 2)
print(myRectangle.area)

// property observer
class CaloriesBurned{
    var totalCaloriesBurned : Int = 0{
    willSet(newCalorieBurned){
        print("totalCaloriesBurned is going to added with \(newCalorieBurned)") // willSet wil be called even the oldValue is not changed.
    }
    didSet(oldCalorieBurned){
        if totalCaloriesBurned > oldCalorieBurned{
            print("Calories burned is \(totalCaloriesBurned-oldCalorieBurned)") //didSet will be called only if the oldValue is changed.
        }
    }
    }
}

var calorieTracker = CaloriesBurned()
calorieTracker.totalCaloriesBurned = 100
calorieTracker.totalCaloriesBurned = 150
calorieTracker.totalCaloriesBurned = 250
calorieTracker.totalCaloriesBurned = 250 // Here the didSet will not be called... only the willSet will be called.

//propert wrapper
@propertyWrapper struct IsAlwaysFive {
    private var x : Int = 0
    var wrappedValue:Int{
        get{
           return x
        }
        set{
            
           x = 5
            
        }
    }
}

// NOTE ---- the property can only be used within a structure or a class

struct NewStruct{
@IsAlwaysFive var height: Int  //always changes the height value to 5
    init(){
        height = 0
    }
}
var heightStruct = NewStruct()
print(heightStruct.height)

heightStruct.height = 1
print(heightStruct.height)

struct SmallRectangle {
    private var _height = IsAlwaysFive()
    private var _width = IsAlwaysFive()
    var height: Int {
        get { return _height.wrappedValue }
        set { _height.wrappedValue = newValue }
    }
    var width: Int {
        get { return _width.wrappedValue }
        set { _width.wrappedValue = newValue }
    }
}

var newSmall = SmallRectangle()
newSmall.height = 5
print(newSmall.height)

/* Float accuracy is over 6 digits and Double accuracy is over 15 Digits */
var myAge = "21"
var myAgeInFloat = 21.000
var myAgeConInt = Int(myAgeInFloat)
print(myAgeConInt)

// mutating Func is only for structure and not for classes and mutating func cannot be used for normal functions
struct Swapping{
    var x :Int
    var y : Int
    init(){
        x=0
        y=0
    }
mutating func addValuesFor (x delx : Int,y dely : Int,by value :Int){
        x = delx
        y = dely
        x += value
        y += value
        }
}

var newSwap = Swapping()
newSwap.addValuesFor(x: 5, y: 6, by: 2)
print(newSwap.x,newSwap.y)

// Subscript

struct FiveTimesValueOf{
    var multipier:Int
    subscript(Multipier: Int) -> Int{
        Multipier * 5
    }
    
}

var threeTimes = FiveTimesValueOf(multipier: 3)
print(threeTimes.multipier)
print(threeTimes[5])

// generics are templates in C

func swapTwoValues<T>(a :inout T, b: inout T){
    let temp = a
    a = b
    b = temp
}

var x = "Hello"
var y = "Hai"
print(x,y)
swapTwoValues(a: &x, b: &y) // while passing address the variable must be of var type not a constant
print(x,y)

var n = 21.000
var m = 23.000
print(n,m)
swapTwoValues(a: &n, b: &m)
print(n,m)

// a generic stack
struct Stack<dataType>{
    var item = [dataType]()
    mutating func push(of element: dataType){
        item.append(element)
    }
    mutating func pop()->dataType{
        let last = item[item.endIndex-1]
        item.removeLast()
        return last
        
    }
    mutating func topElement()->dataType{
        let top : dataType? = item.isEmpty ? nil:item[item.endIndex-1]
        return top!
    }
    mutating func count()->Int{
        return item.count
        
    }
}

var myStack = Stack<String>()
myStack.push(of: "hello")
myStack.push(of: "hai")
print(myStack.topElement())
print(myStack.pop())
print(myStack.topElement())
print(myStack.count())
print(myStack.pop())
//print(myStack.topElement())
