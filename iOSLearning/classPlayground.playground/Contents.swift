import UIKit

class Person {
    var salary:Int = 0
    var name:String = ""
    var occupation:String = ""
    
    func idle(){
        print("\(name) is IDLE")
    }
    
    func busy(){
        print("\(name) is BUSY")
    }
    func getSalary(){
        print("\(name) salary is \(salary)")
    }
    func knowOccupation(){
        print("\(name)'s occupation is \(occupation)")
    }
}
class Car: Person{
    
}
var people1 = Person()
people1.name = "Arun"
people1.salary = 20000
people1.occupation = "Manager"

people1.busy()
var people2 = Person()
people2.occupation = "teacher"
people2.salary=10000
people2.getSalary()
people2.knowOccupation()

var employee = Array<Person>()

